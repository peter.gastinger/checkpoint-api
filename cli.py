from pprint import pprint

from checkpoint_api.checkpoint import CheckpointSession

# with CheckpointSession("10.100.30.25", "api", "CGx3IjUSd47TrXKh6zJ7", read_only=False) as cps:
with CheckpointSession(
    "10.200.30.25", api_key="Y1oyNmi1SA7GeGpgmlgKCQ==", read_only=False
) as cps:
    #    data = {'name': 'IP_10.14.33.6_xlnetp02', 'details-level': 'standard'}
    #    response_json = cps.post("show-host", json=data)
    #    pprint(response_json)
    # response_json = cps.search_object_by_cidr("10.14.32.0/24")
    #    response_json = cps.post('show-objects', json={'filter':"10.14.32.0/24", 'limit':10})
    #    pprint(response_json)
    #    pprint(len(response_json))

    #    response_json = cps.search_object_by_cidr("10.14.32.0/24")
    #    pprint(response_json)
    #    pprint(len(response_json))

    #    response_json = cps.search_object_by_cidr("10.14.33.6")
    #    pprint(response_json)

    #    response_json = cps.post('add-host', json={'name': 'gap-test-01', 'ip-address': '1.2.3.4'})
    #    pprint(response_json)

    #    response_json = cps.publish()
    #    pprint(response_json)

    #    response_json = cps.policy_install('fw-brd-policy', 'fw-brd' )
    #    pprint(response_json)

    #    response_json = cps.post_paginate('show-updatable-objects-repository-content', json={})
    #    pprint(response_json)
    #    pprint(len(response_json))

    #    response_json = cps.post('show-packages', json={} )
    #    pprint(response_json)

    #    print(cps.post('show-api-versions', json={}))

    #    print(cps.post('show-unused-objects', json={}))

    #    id = cps.post('show-changes', json={})
    #    pprint(cps.post('show-package', json={'uid':'8917f4f1-36cc-48ed-9e58-1f02a030e2db'}))
    # pprint(cps.post('show-nat-rulebase', json={'package':'fw-brd-policy'}))

    #    pprint(cps.post('show-package', json={'name': 'fw-brd-policy'}))
    # -> access-layers name
    #    rulebase = cps.post_paginate('show-access-rulebase', json={'package': 'fw-brd-policy', 'name':'fw-brd-policy Network', 'show-hits': True })

    #    print(rulebase["objects-dictionary"])

    #    pprint(cps.post('show-data-center-servers', json={}))
    #    pprint(cps.post('show-data-center-objects', json={}))
    #
    #    pprint(cps.post('show-data-center-server', json={'name':'NSX-W01', 'details-level':'full'}))

    #    pprint(cps.post('add-data-center-object', json={'data-center-name':'NSX-W01', 'uid-in-data-center':'5cf01e6f-9b94-4462-bea9-bed7a5f2967a'}))

    pprint(
        cps.post(
            "show-data-center-object",
            json={"name": "default.GAP-DOKU", "details-level": "full"},
        )
    )

    pprint(
        cps.post(
            "add-access-rule",
            json={
                "layer": "fw-brd-policy Network",
                "position": {"bottom": "AUTOMATIC_RULE_SECTION"},
                "name": "brd_auto_rule_01",
                "action": "Accept",
                "comments": "2020-10-23--15:24--wrzgap--",
                "destination": ["10.0.0.0/8"],
                "source": ["default.DOKU"],
                "service": ["https"],
                "track": "Log",
            },
        )
    )

#    pprint(cps.publish())


# https://10.15.104.75/api/v1/ns-groups
# https://10.15.104.75/api/v1/ns-groups/75ba6aa3-cc5b-4f52-a980-d18d80411678


#    pprint(cps.post_paginate('show-https-rulebase', json={'package': 'fw-brd-policy', 'name':'Default Layer','use-object-dictionary':True }))
#    pprint(cps.post('show-server-certificates', json={}))

#    cert = "MIIQyQIBAzCCEI8GCSqGSIb3DQEHAaCCEIAEghB8MIIQeDCCBqcGCSqGSIb3DQEHBqCCBpgwggaUAgEAMIIGjQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQI8GCILjYl/VYCAggAgIIGYAOQsuRB0IkfsSYOYR8H0ItVHLqiHVGY5dZZ2PCIWvkMJCKv+u0E96TcaKWrigha5hIyrj2bQghAtIh/vRT1lwSdjBpm5JjHIiPRE9QG0lfdKRYwNK4ywtylCI/08W+0uo+hR+MfzIUIkFblVC+MxuA9ojqgExiuIhGx2eEik3Kauh/X10p4EAbYlxgI+vkDUvNAAfKSB/1o3fa4cW04cZs5c1fvYqPK8AnTWI9mPhx7Lkla2XEaskeyjJCZ+0jLgsWXsoiXM+AS0SLA/yByEz2E0/M6CLOKWcPA9aE4bAMHOmavVxEDso2Jsx0AdrSVocKyM/aZ5xBLZgyhw9lJryUanvle8TqcLeRbiDVRHVSDMcbbk22hpLxoiiYG708umIQzGVILOyhecemuNwUlF8s3qExA/0rU+8pFs7Ya3N9erOw4iT55PyPL6ev2s/uJSYCY4grGrBnU37MflBEPISi3TMaUuQMbLPyo4/BMqcSbEx9Emuh7NLOxKYGte86l6pP+Dp1gvfoCxgaRGMy3RvGdLuhXdL+t5POX4jTAJ0R6+prhPaJidtUCaUnV8LUw2nhmkWRMhTMzTPcr7RHoiTePQAvB472fgQ/FsPViF62q6ppE2cBVHwqajwlJcAbbPDhn4sNkaM9CwbKpDAMx6gcdWQ8BAJXPH6CW6X1C16ECCmPlK5wBBXfZjkaJz2HQp7+mN76k0yvZ4+GvV68GrWQ6LwI4nuXRPSj8IXnBZyq/MQQYhiKumC4oGYwaC3cDXzdKhNrIUgTqcuvTQdBWmN6h925/4Dv4p6HmC/B2Eny0XsJKtAJsBZHrcFv9NWHQ4kHiP4O2wcHMYi9kgPhQfZbtFVj2EfaOlMvZRSeaaZsYeJzN/akYvYe/E5mgNuBIq3kazKm5GCV+ADLHo8yP1IfOaF58Dw8su+6h6IBFca91C3K33A2zAAOK47+S7W2mdJkm+mQ/4NU1GiWCjCY3gXVdxiv0i130iVCsm/B9wkVT/34N39B875XD7IExXDJ5FswioNA/S4wKVu7POUr8I8thx5djPwv9XNxZgamXJ9m/8F/OEtMVWFNJKgbXIQGl0HlezOx04vZ2OTTK0JAxEl75GZ++hu90blEMnH3+qrxcuF5FkNZ9UU21dQwAfpRKfrSyfszVR3sIHEoXkuElqHnZFhJhXff3a2335Qvwx5esnAv3mLKj76BqdAgipWjvD35gySvE/bMTj8ApbT2yTouLQI1fdPkPKbCkOAz6h4621dYwNBOALPgB2y6LgT4ZCEytlSUPUC4uPaY27HOb+s1Gvh/LwyF2TMW3pYpss2R25EfUFBVBJytB5cV3fn+14Q3P6IFRnsD5ki6a8DqxzNJzYPJI34HcO/ATC3d09+imCBsjYQ4kOsHBgz2pLLaz1nJE35K1EQamCj0Ck80wNIVXUvBeYXGGp6RiTtoc8vCsTOYBl3Z8MESJL6FiM8OG0fXfQuvptsepjdj41/TqPPTaIFHlZ/vz14F/ZZtITyvOy6N3U0FAqDIjAONFe20vKre3FbX5LqenDM1ucqauerfapheiTpxdNYWHN42q3pDhj8X7vMHKlh/ixNPWDQmdqDC3FIbwdKG/6+Pe+Xc+UV6ruY99N2T+e067Gdz9mSiZddHm4pd1Nb/lOj+44ddwR4b8j5LHsmo3y7aDYHQiu3eBEzqY6MTP18nDJJ56zSNhKibYfCkCBwSLTAjcqcx4RTvg9N13qT4xFgLJGhlC7ajrPcgxmBM6Wm4rNYxtXk5c+/E00DGJ6pY4L+bTj1fdahNSP1pLdJzJhWvRrYl4RD60Wa08q7DJVPCYj6WtSZvRNzuSYctov6TnASbKHcr0G4kRWg/KX/i9DV4C1SBV5Y7C/QtuiQrsBIoZcf+e6umBqpKmQ/DVO3V3MEYyi/LCmjphDFctoPAnlwZqibBdLosRHk7y99KcoCAz+I+DwoBo/RAAywjTjqaAWNO4wUFuUCaqBQG6Wr/Om5Q8mrOpc03m6u7ooVQkshVmSJJFrQWzi4z8v45k0k/3xrvgCFpUEV90UFX1TA6U9tpUlXuTzj2iyb5cNVXXkbkQBZ2jDFzOc+dKc3V4L8cK6bzN5+Mim+3Ad3zNxCIxix7gQSGaNttQgSMyNlhjtn+EL/NK2PXhgMDyFzs0fz5e4FEJyTGDPDCCCckGCSqGSIb3DQEHAaCCCboEggm2MIIJsjCCCa4GCyqGSIb3DQEMCgECoIIJdjCCCXIwHAYKKoZIhvcNAQwBAzAOBAjMb9QM4HEznwICCAAEgglQGhGVK5Hx9+9KRMeSp7+IVcCsyo5H/+cm2SOPH6JMMW1uCTrk6ZpLWKuL8kLyFS+CCS8CRBKqDCcQ8d5oFZHaebyec+jmubhJujuNmnhbEWcL3WK2twl8YlSb3nkn7tv3j3BQ89j7s3DE3NMuTDK7Zpnnmo+IyEybdFFWhhptG2AkGHw/34Om+xT0O1DhpYxmyGUjSi1borEtSC6M1bjOKq8MpT1vdPgIs+AZuovmAVZx6KVAZUq/K6SgrZB/Lh2rRJUkXUSRQhVG/BlKtTJP+8iUnmoHknmwU7miHc7HsRkDeicqOc61BVe0b8vRCGus7F0q3YWOmgtsFuaQ89iojKakyjeG9ndW+pxezwwoFv1ysJNSugJZJJJbSJDiX2Bu4oIuhdByzIQW7I7Ed+1BNMkM43qD3CMBWkKZJyZMS1i95mPOAnl0mZ40PQ+n4I3/LNDmXOdSXxI5IvbmzR592DnQF/nT+Avjg1xNtJrCXWR4G8n5BWGl6SNcZS1KGTeUJlyRgH597g87pCl082Dqi7s70fd9vCO/6JOhg8dEvahrChbA3glzmcy8u4fpeWXOsld2QwFo/57VolWE0EDhWCa+LilwNGFqc2ACD0ZxQIq1yDexpkcENvkQQVz2pl6Hc7c3c99UDAiSRIY697nz4s10uqW3OGn643JJrMDdxld99cPc53zNxf+umCKL/vLDqJtNzwAP2b4xiHQLaACfwVp/vZ8qnNHEY+QLZshOpVsQ8U1mrToAtHAGDEK4Bh0de3wsUIZa3HPWxWpphncKNN2E5r921PTQJSXaZfGoK1UZQ+mpeEkVuOBJsLGs8NRKWXv4WVQirfmQ4qbhE/GG0YRjdt1EUCo7NPjKQfwYXaoR22ldbGtEGycxA2Jvts45gGzuyXBPFEKF9nFQhaPMV6rTjVpAJlDQUkTlA905OROgNaDip+/67JyMumVTy82LVPXaYDLNOATgoX0RcoG3wICUZzLvxnenGd9RxgiVOaE+uharzsXFcnZVw3avBNjQc/ipSQdQnsND4JhvL+KwCxfpAArEd8dG3Xqt7uLis2S2Zy+Bwub1S0u1FqpuxtM1jzouwzutHF/U8ev6NG9ErRRSZm8+fVAzyF1x+CDv3DBV59W+cMAknIqd2Fo/C8+Dc+l0MUFn4ccfsJ8ao43PIduyqEHt/EUvRi3kwKHgBkIgdaNeJ3NWOh3zF3MNi19tkVzWHYAhJtD1P7JHnkSbavk+aEs1+DB+dojioNte0/kRN2dwCu2C8zPmeGWY2O5X1GcFTWOIjis4nGg/L+w45w+Tu47nsvnlznAgygiF8b1GyrQQxZ+dEtGI84x1s1Pfo4Yiumq2iQ5j2TFlAiDi+m2ykAYZ/vpbYCXFXqAMQg4U5YAVuxBZ5J4/XF3116/3hKCmj0pnqp6TBp+lgJzpKDqIwoJ7AQFPqHGmj9NoWKY88LZn19eX/Dz5hXAMBhkG/Io6RzreQYm7DjhcCmk2bk4jJ+YLTrFlAiq/YYiVNmz/GzGBNUABOb1ceTk2U1nFyxrng5sIIuVGbFp/BgntXpli8d2SiT9iamSQCwlNMTVZeB87vnVWVUbp/brfKtnJ47LFYVO3coZN0WlhCc+T14nkyVYSxBoIsQfUNXRS+fvF1B0iFtZrhAZ7LNygzmpccSds3cCBWJe4RiMxRDBckYpDiSA/Ad66ZqN3uCjOBMiksBDbmIyxqTA8R/HvpFndrudLBoP+F0lbyp60UzknqFDJbe5YFTGk6tkfgYn/rPE38jSAT/ui9wUIPlPuvQru05eUcOadprhLBXmDrZHNnpOeMnwm6PUWle2lvPVbyO020kNbRufwRfv2F/5YZK7+R+d9D/jsAthmrl6UXXym8xX4CKNu2e9qJnK99r/tg7Ncrgk+wDz9uO98lWJNrl4SoVKFHNDSy+VDDkwKpixPGRhUVoEAvDnZrGAw3vT5PC2M+6Rdg3ZM3/yswVgbjweUHT0c+budnYOP+mpxNReW5OKN5oeD15w+fYsL2un0aqBVIoD928gvH2Ns3ta1maab9f+1bUnbX6oYmHo+NCMdCdINsMrEZb4l2ICP9hINwFfbtGh4W8qu0NoptWE/T4/awG+ln/u7+OEuEih87LuQXCU/oW8CaAHgZpiFo48HoviOCrJ6BT92DrguADk1538K5TjH0ohrsD4nXT3h+XlmsAcNw/cM1CjTiSlv6mFtxTIi9Vf5hf6IaUrUil9imPPCvxJcJI7I5kMEtZ0ihbU+Z1gZT8Xbg4/UqFGKrsHR3A+ylOMHWgDC6mPLe/jYdv/bfG8H5NOLy/mqkYn2pFn5GG9nGPo5WT1MBUl96S//MKtyFnC+p8yOxtQBasDUQmFenejJ42YJ/WXDPl8IGkeEU9X3Es9gDcFGmfOQMflnl/UOR7mwUMZCYFrUvKl3O2epcP8kRbrMfo5Oj4+8AmfsucomoAwoBHpoLWJBgnIUJZLDUqvcPVWvt5RZXjqtZ63qcCkntmmD/RrHXC9cXVbBNWHh1NLzChe6dP+KeekRZ9P8NX7H9sRGK5/xHNUdlZDMIoXL1nQcRMAJsI6h98tj+LbdAQ1tR53F3SvC4spzrIZ46YTY+SqZTTvhI/L/NqOvBPRwLHVlgaaCjNK36YQ0Ny60TcmH8pGSbsbioGblBZWF86ODpHG/vSftMbFTiBZ+Xre7jk3+v4QB6CpL6pxL5+NdnqyoRFR/4DX78fVMpH4d6zoqP/8AJSXfpLb0wAWnFVdZkd97VlU5Q64t8EkFIYK6NGJJDhpGNQktz2zCEqBppKysRgHJhTNBMK+u0MV10FSi8EvS2AAtESA83oOGP3JCj38EAXii07LFRZQvsljnQ/Tu+xEgO9wKFBuJEK8cNSywCE3UcraFFdQyEr0WWFfKQsglxDrok3UPbk6TF9vp4JAJIRn48FXp22Jhr6X0Dsf1gd+A5IDms92t/sO3zLX9V2149gadDRmw9wL8p74WugYXt/iYbLqAMnAj6sAHhoVwlyGg0EegZNRvPNzprnGe7OkqKkjc+v4Fxe6g8kvkNJ+4JmZyFIjHU7oTVRIGR8XIZiPrvB36V/SQarxWlS0pIFbVE4ErVpvhK877aJQwNdi3eCB6JG7PTg9MrCIf6TFkQJFccJ980eiPhE0MdU3VO2uiEwN3ye36TnM3A+AxJTAjBgkqhkiG9w0BCRUxFgQU7Pws+23kJ6MWex1Wbl1X4GNRJlowMTAhMAkGBSsOAwIaBQAEFL1/L0k4dwmWqLSP1nulHF2Kf/ChBAj0+C/ToAOx0QICCAA="
#    pprint(cps.post('set-server-certificate', json={'name':'mydefaultcert', 'base64-certificate':cert, 'base64-password':'UmFpZmZlaXNlbjEyMyE='}))
#    pprint(cps.publish())
#    pprint(cps.post('show-updatable-objects', json={}))

#    pprint(cps.post('show-simple-gateways', json={}))


# with CheckpointSession("awrzgap", "pw", "10.122.0.110") as cps:
#    res = cps.post_paginate('show-administrators', json={'details-level':DetailsLevel.FULL.value})
#    pprint(res)
#    pprint(len(res))
