import dataclasses
import pickle

from checkpoint_api.checkpoint import CheckpointSession
from openpyxl import Workbook
from openpyxl.styles import Font, Alignment

get_values_from_cp = False


class CheckPointRuleBase(object):
    def __init__(self, json_rulebase_export, resolve_objects=False):
        self.resolve_objects = resolve_objects
        self.rulebase = list()
        objects_dict = {
            obj["uid"]: obj for obj in json_rulebase_export["objects-dictionary"]
        }
        for item in json_rulebase_export["rulebase"]:
            if item["type"] == "access-section":
                for rule in item["rulebase"]:
                    self.split_rule(rule, objects_dict, item["name"])
            else:
                self.split_rule(item, objects_dict)

    def resolve_object(self, obj):
        mapping = {
            "host": "ipv4-address",
            "network": ["subnet4", "mask-length4"],
        }
        try:
            if obj["type"] in mapping:
                if obj["type"] == "network":
                    return f'{obj["subnet4"]}/{obj["mask-length4"]}'
                return obj[mapping[obj["type"]]]
        except:
            pass
        return obj["name"]

    def lookup_name_for_key(self, key, rule, objects_dict, resolve_objects=False):
        assert key in rule
        obj_list = list()
        if type(rule[key]) == list:
            for obj in rule[key]:
                if obj in objects_dict:
                    if resolve_objects:
                        if objects_dict[obj]["type"] == "group":
                            for member in objects_dict[obj]["members"]:
                                obj_list.append(self.resolve_object(member))
                        else:
                            obj_list.append(self.resolve_object(objects_dict[obj]))
                    else:
                        obj_list.append(objects_dict[obj]["name"])
            return obj_list
        else:
            if rule[key] in objects_dict:
                if resolve_objects:
                    if objects_dict[rule[key]]["type"] == "group":
                        for member in objects_dict[rule[key]]["members"]:
                            obj_list.append(self.resolve_object(member))
                    else:
                        obj_list.append(self.resolve_object(objects_dict[rule[key]]))
                else:
                    return objects_dict[rule[key]]["name"]

    def split_rule(self, rule, objects_dict, section=""):
        assert rule["type"] == "access-rule"

        rule_class = CheckPointRule(rule)
        rule_class.section = section
        rule_class.no = rule["rule-number"]
        rule_class.enabled = rule["enabled"]
        rule_class.name = rule.get("name", "")
        rule_class.uid = rule["uid"]
        rule_class.comments = rule["comments"]
        rule_class.time = self.lookup_name_for_key(
            "time", rule, objects_dict, self.resolve_objects
        )
        rule_class.src_negate = rule["source-negate"]
        rule_class.src = self.lookup_name_for_key(
            "source", rule, objects_dict, self.resolve_objects
        )
        rule_class.dst_negate = rule["destination-negate"]
        rule_class.dst = self.lookup_name_for_key(
            "destination", rule, objects_dict, self.resolve_objects
        )
        rule_class.svc_negate = rule["service-negate"]
        rule_class.svc = self.lookup_name_for_key(
            "service", rule, objects_dict, self.resolve_objects
        )
        rule_class.content_negate = rule["content-negate"]
        rule_class.content = self.lookup_name_for_key(
            "content", rule, objects_dict, self.resolve_objects
        )
        rule_class.content_direction = self.lookup_name_for_key(
            "content-direction", rule, objects_dict, self.resolve_objects
        )
        rule_class.vpn = self.lookup_name_for_key(
            "vpn", rule, objects_dict, self.resolve_objects
        )
        rule_class.action = self.lookup_name_for_key(
            "action", rule, objects_dict, self.resolve_objects
        )
        # rule_class.hits = rule["hits"]["value"]
        try:
            rule_class.hits = CheckPointRuleHits(
                rule["hits"]["value"],
                rule["hits"].get("last-date", {}).get("iso-8601", ""),
                rule["hits"].get("first-date", {}).get("iso-8601", ""),
                rule["hits"].get("level", ""),
                rule["hits"].get("percentage", ""),
            )
        except:
            rule_class.hits = None
        rule_class.install_on = self.lookup_name_for_key(
            "install-on", rule, objects_dict, self.resolve_objects
        )

        rule_class.meta = CheckPointRuleMeta(
            rule["meta-info"]["creator"],
            rule["meta-info"]["creation-time"]["iso-8601"],
            rule["meta-info"]["last-modifier"],
            rule["meta-info"]["last-modify-time"]["iso-8601"],
        )
        rule_class.track = CheckPointRuleTrack(
            rule["track"]["type"],
            rule["track"]["per-session"],
            rule["track"]["per-connection"],
            rule["track"]["accounting"],
            rule["track"]["enable-firewall-session"],
            rule["track"]["alert"],
        )

        self.rulebase.append(rule_class)

    def __str__(self):
        return "\n".join([str(rule) for rule in self.rulebase])


class CheckPointRule(object):
    def __init__(self, rule):
        self._rule = rule

    def __str__(self):
        return f"{self.section=};{self.no=};{self.hits};{self.name=};{self.src=};{self.dst=};{self.svc=};{self.action=};{self.track=};{self.comments=};{self.meta=}"


@dataclasses.dataclass()
class CheckPointRuleMeta:
    creator: str
    creation_time: str
    last_modifier: str
    last_modify_time: str

    def __str__(self) -> str:
        return f"created by {self.creator} on {self.creation_time}, last_modified by {self.last_modifier} on {self.last_modify_time}"


@dataclasses.dataclass()
class CheckPointRuleHits:
    value: str
    last_date: str
    first_date: str
    level: str
    percentage: str

    def __str__(self) -> str:
        if self.last_date:
            return f"{self.value} (last: {self.last_date})"
        else:
            return f"{self.value}"


@dataclasses.dataclass()
class CheckPointRuleTrack:
    type: str
    per_session: bool
    per_connection: bool
    accounting: bool
    enable_firewall_session: bool
    alert: str

    def __str__(self) -> str:
        return f"log: {self.per_connection} accounting: {self.accounting}"


def update_headers(ws):
    for cell in ws[1]:
        cell.font = ft
        cell.alignment = Alignment(horizontal="center")


def set_column_width(ws):
    for column_cells in ws.columns:
        length = max(len(str(cell.value)) for cell in column_cells) + 4
        ws.column_dimensions[column_cells[0].column_letter].width = length
    ws.auto_filter.ref = ws.dimensions


# get values from
policy = "Blackout-Border-AT"
if get_values_from_cp:
    with CheckpointSession(
        "10.113.255.200",
        checkpoint_user="adminpeter210",
        checkpoint_pass="c%uqhg$X_Z8LXseM",
        domain="WW-Internet-Borders",
    ) as cps:
        json_rulebase_export = cps.post_paginate(
            "show-access-rulebase",
            json={
                "package": policy,
                "name": "Network-Layer-AT",
                "show-hits": True,
                "dereference-group-members": True,
                "show-membership": True,
                "details-level": "full",
            },
        )
        pickle.dump(json_rulebase_export, open("fw.pkl", "wb"))
#        pprint(res)
#    for item in res["access-layers"]:
#        print(f'\t{item["name"]}')

# pprint(res)
# break
else:
    json_rulebase_export = pickle.load(open("fw.pkl", "rb"))

# generate xls
wb = Workbook()
ft = Font(bold=True)
ws = wb.active
ws.title = policy
ws.append(
    [
        "section",
        "no",
        "enabled",
        "hits",
        "last_hit",
        "name",
        "uid",
        "src",
        "dst",
        "svc",
        "action",
        "track",
        "comments",
        "meta",
    ]
)

update_headers(ws)

# resolve_objects does not work for global objects
for rule in CheckPointRuleBase(json_rulebase_export, resolve_objects=False).rulebase:
    ws.append(
        [
            rule.section,
            rule.no,
            rule.enabled,
            rule.hits.value,
            rule.hits.last_date,
            rule.name,
            rule.uid,
            ",".join(rule.src),
            ",".join(rule.dst),
            ",".join(rule.svc),
            rule.action,
            str(rule.track),
            rule.comments,
            str(rule.meta),
        ]
    )

set_column_width(ws)
wb.save("rule_base_export.xlsx")
