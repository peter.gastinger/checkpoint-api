class CheckpointUnsupportedTypeException(Exception):
    pass


class CheckpointMissingCredentialsException(Exception):
    pass


class CheckpointTooManyCredentialsException(Exception):
    pass
