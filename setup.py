import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="checkpoint_api",
    version="0.1.8",
    author="Peter Gastinger",
    author_email="peter.gastinger@gmail.com",
    description="Check Point REST API abstraction",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    install_requires=[
        "requests",
    ],
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
